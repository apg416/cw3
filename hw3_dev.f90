!Aleksander Grzyb 01072956
!M3C 2018 Homework 3
!This module contains four module variables and two subroutines;
!one of these routines must be developed for this assignment.
!Module variables--
! tr_b, tr_e, tr_g: the parameters b, e, and g in the tribe competition model
! numthreads: The number of threads that should be used in parallel regions within simulate2_omp
!
!Module routines---
! simulate2_f90: Simulate tribal competition over m trials. Return: all s matrices at final time
! and fc at nt+1 times averaged across the m trials.
! simulate2_omp: Same input/output functionality as simulate2.f90 but parallelized with OpenMP

module tribes
  use omp_lib
  implicit none
  integer :: numthreads
  real(kind=8) :: tr_b,tr_e,tr_g
contains

!Simulate m trials of Cooperator vs. Mercenary competition using the parameters, tr_b and tr_e.
!Input:
! n: defines n x n grid of villages
! nt: number of time steps
! m: number of trials
!Output:
! s: status matrix at final time step for all m trials
! fc_ave: fraction of cooperators at each time step (including initial condition)
! averaged across the m trials
subroutine simulate2_f90(n,nt,m,s,fc_ave)
  implicit none
  integer, intent(in) :: n,nt,m
  integer, intent(out), dimension(n,n,m) :: s
  real(kind=8), intent(out), dimension(nt+1) :: fc_ave
  integer :: i1,j1
  real(kind=8) :: n2inv
  integer, dimension(n,n,m) :: nb,nc
  integer, dimension(n+2,n+2,m) :: s2
  real(kind=8), dimension(n,n,m) :: f,p,a,pden,nbinv
  real(kind=8), dimension(n+2,n+2,m) :: f2,f2s2
  real(kind=8), allocatable, dimension(:,:,:) :: r !random numbers


  !---Problem setup----
  !Initialize arrays and problem parameters

  !initial condition
  s=1
  j1 = (n+1)/2
  s(j1,j1,:) = 0

  n2inv = 1.d0/dble(n*n)
  fc_ave(1) = sum(s)*(n2inv/m)

  s2 = 0
  f2 = 0.d0

  !Calculate number of neighbors for each point
  nb = 8
  nb(1,2:n-1,:) = 5
  nb(n,2:n-1,:) = 5
  nb(2:n-1,1,:) = 5
  nb(2:n-1,n,:) = 5
  nb(1,1,:) = 3
  nb(1,n,:) = 3
  nb(n,1,:) = 3
  nb(n,n,:) = 3

  nbinv = 1.d0/nb
  allocate(r(n,n,m))
  !---finished Problem setup---


  !----Time marching----
  do i1=1,nt

    call random_number(r) !Random numbers used to update s every time step

    !Set up coefficients for fitness calculation in matrix, a
    a = 1
    where(s==0)
      a=tr_b
    end where

    !create s2 by adding boundary of zeros to s
    s2(2:n+1,2:n+1,:) = s

    !Count number of C neighbors for each point
    nc = s2(1:n,1:n,:) + s2(1:n,2:n+1,:) + s2(1:n,3:n+2,:) + &
         s2(2:n+1,1:n,:)                  + s2(2:n+1,3:n+2,:) + &
         s2(3:n+2,1:n,:)   + s2(3:n+2,2:n+1,:)   + s2(3:n+2,3:n+2,:)

    !Calculate fitness matrix, f----
    f = nc*a
    where(s==0)
      f = f + (nb-nc)*tr_e
    end where
    f = f*nbinv
    !-----------

    !Calculate probability matrix, p----
    f2(2:n+1,2:n+1,:) = f
    f2s2 = f2*s2

    !Total fitness of cooperators in community
    p = f2s2(1:n,1:n,:) + f2s2(1:n,2:n+1,:) + f2s2(1:n,3:n+2,:) + &
           f2s2(2:n+1,1:n,:) + f2s2(2:n+1,2:n+1,:)  + f2s2(2:n+1,3:n+2,:) + &
          f2s2(3:n+2,1:n,:)   + f2s2(3:n+2,2:n+1,:)   + f2s2(3:n+2,3:n+2,:)

    !Total fitness of all members of community
    pden = f2(1:n,1:n,:) + f2(1:n,2:n+1,:) + f2(1:n,3:n+2,:) + &
           f2(2:n+1,1:n,:) + f2(2:n+1,2:n+1,:)  + f2(2:n+1,3:n+2,:) + &
          f2(3:n+2,1:n,:)   + f2(3:n+2,2:n+1,:)   + f2(3:n+2,3:n+2,:)


    p = (p/pden)*tr_g + 0.5d0*(1.d0-tr_g) !probability matrix
    !----------

    !Set new affiliations based on probability matrix and random numbers stored in R
    s = 0
    where (R<=p)
        s = 1
    end where

    fc_ave(i1+1) = sum(s)*(n2inv/m)

  end do

end subroutine simulate2_f90

!Simulate m trials of Cooperator vs. Mercenary competition using the parameters, tr_b and tr_e.
!Same functionality as simulate2_f90, but parallelized with OpenMP
!Parallel regions should use numthreads threads.
!Input:
! n: defines n x n grid of villages
! nt: number of time steps
! m: number of trials
!Output:
! s: status matrix at final time step for all m trials
! fc_ave: fraction of cooperators at each time step (including initial condition)
! averaged across the m trials
subroutine simulate2_omp(n,nt,m,s,fc_ave)
  implicit none
  integer, intent(in) :: n,nt,m
  integer, intent(out), dimension(n,n,m) :: s
  real(kind=8), intent(out), dimension(nt+1) :: fc_ave
  integer :: i1,j1
  real(kind=8), allocatable, dimension(:,:,:) :: r !random numbers
  !Add further variables as needed

  real(kind=8) :: n2inv
  integer, dimension(n,n,m) :: nb,nc
  integer, dimension(n+2,n+2,m) :: s2
  real(kind=8), dimension(n,n,m) :: f,p,a,pden,nbinv
  real(kind=8), dimension(n+2,n+2,m) :: f2,f2s2
  real(kind=8), dimension(nt+1,m) :: fc_ave_mat
  integer :: im

  !initial condition and r allocation (does not need to be parallelized)
  s=1
  j1 = (n+1)/2
  s(j1,j1,:) = 0
  allocate(r(n,n,m))
  !------------------




  !Add code here
  !$ call omp_set_num_threads(numthreads)
  n2inv = 1.d0/dble(n*n)
  fc_ave_mat(1,:) = 1-(n2inv)
  s2 = 0
  f2 = 0.d0
    nb = 8
  !$OMP parallel
  !$OMP sections

  !$OMP section
    nb(1,2:n-1,:) = 5
  !$OMP section
    nb(n,2:n-1,:) = 5
  !$OMP section
    nb(2:n-1,1,:) = 5
  !$OMP section
    nb(2:n-1,n,:) = 5
  !$OMP section
    nb(1,1,:) = 3
    nb(1,n,:) = 3
    nb(n,1,:) = 3
    nb(n,n,:) = 3

  !$OMP end sections


    nbinv = 1.d0/nb

!Initialization of nb can be parallelized as each thread is modifying different entries of the matrix.

!Because each consecutive iteration depends on the previous one in simulate2 we cannot simply parallelize
!the do loop. Nevertheless, we are simulating m independent realizations of the process (m parallel universes!),
!so each thread can be assigned a fraction of them. We modify simulate2_f90 so that instead of comnplete vectorization we
!do the operations in m-dimension in a parallel loop. As we are doing the fork anyway, nothing prevents us from initializing
!nb matrix in parallel.
!---------------------------------------------
!----Time marching----
!$OMP do
do im=1,m
do i1=1,nt

  call random_number(r(:,:,im)) !Random numbers used to update s every time step

  !Set up coefficients for fitness calculation in matrix, a
  a(:,:,im) = 1
  where(s(:,:,im)==0)
    a(:,:,im)=tr_b
  end where

  !create s2 by adding boundary of zeros to s
  s2(2:n+1,2:n+1,im) = s(:,:,im)

  !Count number of C neighbors for each point
  nc(:,:,im) = s2(1:n,1:n,im) + s2(1:n,2:n+1,im) + s2(1:n,3:n+2,im) + &
       s2(2:n+1,1:n,im)                  + s2(2:n+1,3:n+2,im) + &
       s2(3:n+2,1:n,im)   + s2(3:n+2,2:n+1,im)   + s2(3:n+2,3:n+2,im)

  !Calculate fitness matrix, f----
  f(:,:,im) = nc(:,:,im)*a(:,:,im)
  where(s(:,:,im)==0)
    f(:,:,im) = f(:,:,im) + (nb(:,:,im)-nc(:,:,im))*tr_e
  end where
  f(:,:,im) = f(:,:,im)*nbinv(:,:,im)
  !-----------

  !Calculate probability matrix, p----
  f2(2:n+1,2:n+1,im) = f(:,:,im)
  f2s2(:,:,im) = f2(:,:,im)*s2(:,:,im)

  !Total fitness of cooperators in community
  p(:,:,im) = f2s2(1:n,1:n,im) + f2s2(1:n,2:n+1,im) + f2s2(1:n,3:n+2,im) + &
         f2s2(2:n+1,1:n,im) + f2s2(2:n+1,2:n+1,im)  + f2s2(2:n+1,3:n+2,im) + &
        f2s2(3:n+2,1:n,im)   + f2s2(3:n+2,2:n+1,im)   + f2s2(3:n+2,3:n+2,im)

  !Total fitness of all members of community
  pden(:,:,im) = f2(1:n,1:n,im) + f2(1:n,2:n+1,im) + f2(1:n,3:n+2,im) + &
         f2(2:n+1,1:n,im) + f2(2:n+1,2:n+1,im)  + f2(2:n+1,3:n+2,im) + &
        f2(3:n+2,1:n,im)   + f2(3:n+2,2:n+1,im)   + f2(3:n+2,3:n+2,im)


  p(:,:,im) = (p(:,:,im)/pden(:,:,im))*tr_g + 0.5d0*(1.d0-tr_g) !probability matrix
  !----------

  !Set new affiliations based on probability matrix and random numbers stored in R
  s(:,:,im) = 0
  where (R(:,:,im)<=p(:,:,im))
      s(:,:,im) = 1
  end where



fc_ave_mat(i1+1,im) = sum(s(:,:,im))*n2inv

end do
end do
!$OMP end do
!$OMP end parallel
  fc_ave=sum(fc_ave_mat,2)/m
!---------------------------------------------
  deallocate(r)
end subroutine simulate2_omp
!-----------subroutine for animation, very simular to simulate_f90, but returns all intermediate states ---------------------------------------------------
subroutine simulatemov(n,nt,m,evo,s,fc_ave)
  implicit none
  integer, intent(in) :: n,nt,m
  integer, intent(out), dimension(n,n,m) :: s
  integer, intent(out), dimension(n,n,nt+1) :: evo
  real(kind=8), intent(out), dimension(nt+1) :: fc_ave
  integer :: i1,j1
  real(kind=8) :: n2inv
  integer, dimension(n,n,m) :: nb,nc
  integer, dimension(n+2,n+2,m) :: s2
  real(kind=8), dimension(n,n,m) :: f,p,a,pden,nbinv
  real(kind=8), dimension(n+2,n+2,m) :: f2,f2s2
  real(kind=8), allocatable, dimension(:,:,:) :: r !random numbers


  !---Problem setup----
  !Initialize arrays and problem parameters
  evo=0
  !initial condition
  s=1
  j1 = (n+1)/2
  s(j1,j1,:) = 0
  evo(:,:,1)=s(:,:,1)
  n2inv = 1.d0/dble(n*n)
  fc_ave(1) = sum(s)*(n2inv/m)

  s2 = 0
  f2 = 0.d0

  !Calculate number of neighbors for each point
  nb = 8
  nb(1,2:n-1,:) = 5
  nb(n,2:n-1,:) = 5
  nb(2:n-1,1,:) = 5
  nb(2:n-1,n,:) = 5
  nb(1,1,:) = 3
  nb(1,n,:) = 3
  nb(n,1,:) = 3
  nb(n,n,:) = 3

  nbinv = 1.d0/nb
  allocate(r(n,n,m))
  !---finished Problem setup---


  !----Time marching----
  do i1=1,nt

    call random_number(r) !Random numbers used to update s every time step

    !Set up coefficients for fitness calculation in matrix, a
    a = 1
    where(s==0)
      a=tr_b
    end where

    !create s2 by adding boundary of zeros to s
    s2(2:n+1,2:n+1,:) = s

    !Count number of C neighbors for each point
    nc = s2(1:n,1:n,:) + s2(1:n,2:n+1,:) + s2(1:n,3:n+2,:) + &
         s2(2:n+1,1:n,:)                  + s2(2:n+1,3:n+2,:) + &
         s2(3:n+2,1:n,:)   + s2(3:n+2,2:n+1,:)   + s2(3:n+2,3:n+2,:)

    !Calculate fitness matrix, f----
    f = nc*a
    where(s==0)
      f = f + (nb-nc)*tr_e
    end where
    f = f*nbinv
    !-----------

    !Calculate probability matrix, p----
    f2(2:n+1,2:n+1,:) = f
    f2s2 = f2*s2

    !Total fitness of cooperators in community
    p = f2s2(1:n,1:n,:) + f2s2(1:n,2:n+1,:) + f2s2(1:n,3:n+2,:) + &
           f2s2(2:n+1,1:n,:) + f2s2(2:n+1,2:n+1,:)  + f2s2(2:n+1,3:n+2,:) + &
          f2s2(3:n+2,1:n,:)   + f2s2(3:n+2,2:n+1,:)   + f2s2(3:n+2,3:n+2,:)

    !Total fitness of all members of community
    pden = f2(1:n,1:n,:) + f2(1:n,2:n+1,:) + f2(1:n,3:n+2,:) + &
           f2(2:n+1,1:n,:) + f2(2:n+1,2:n+1,:)  + f2(2:n+1,3:n+2,:) + &
          f2(3:n+2,1:n,:)   + f2(3:n+2,2:n+1,:)   + f2(3:n+2,3:n+2,:)


    p = (p/pden)*tr_g + 0.5d0*(1.d0-tr_g) !probability matrix
    !----------

    !Set new affiliations based on probability matrix and random numbers stored in R
    s = 0
    where (R<=p)
        s = 1
    end where

    fc_ave(i1+1) = sum(s)*(n2inv/m)
    evo(:,:,i1+1)=s(:,:,1)
  end do
end subroutine simulatemov

end module tribes
