"Aleksander Grzyb 01072956"
"""M3C 2018 Homework 3
Contains five functions:
    plot_S: plots S matrix -- use if you like
    simulate2: Simulate tribal competition over m trials. Return: all s matrices at final time
        and fc at nt+1 times averaged across the m trials.
    performance: To be completed -- analyze and assess performance of python, fortran, and fortran+openmp simulation codes
    analyze: To be completed -- analyze influence of model parameter, g
    visualize: To be completed -- generate animation illustrating "interesting" tribal dynamics
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from m1 import tribes as tr #assumes that hw3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c hw3_dev.f90 -m m1 -lgomp
#May also use scipy and time modules as needed
import time

def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #C locations
    ind_s1 = np.where(S==1) #M locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.show()
    return None
#------------------


def simulate2(N,Nt,b,e,g,m):
    """Simulate m trials of C vs. M competition on N x N grid over
    Nt generations. b, e, and g are model parameters
    to be used in fitness calculations.
    Output: S: Status of each gridpoint at end of simulation, 0=M, 1=C
            fc_ave: fraction of villages which are C at all Nt+1 times
                    averaged over the m trials
    """
    #Set initial condition
    S  = np.ones((N,N,m),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j,j,:] = 0
    N2inv = 1./(N*N)

    fc_ave = np.zeros(Nt+1) #Fraction of points which are C
    fc_ave[0] = S.sum()

    #Initialize matrices
    NB = np.zeros((N,N,m),dtype=int) #Number of neighbors for each point
    NC = np.zeros((N,N,m),dtype=int) #Number of neighbors who are Cs
    S2 = np.zeros((N+2,N+2,m),dtype=int) #S + border of zeros
    F = np.zeros((N,N,m)) #Fitness matrix
    F2 = np.zeros((N+2,N+2,m)) #Fitness matrix + border of zeros
    A = np.ones((N,N,m)) #Fitness parameters, each of N^2 elements is 1 or b
    P = np.zeros((N,N,m)) #Probability matrix
    Pden = np.zeros((N,N,m))
    #---------------------

    #Calculate number of neighbors for each point
    NB[:,:,:] = 8
    NB[0,1:-1,:],NB[-1,1:-1,:],NB[1:-1,0,:],NB[1:-1,-1,:] = 5,5,5,5
    NB[0,0,:],NB[-1,-1,:],NB[0,-1,:],NB[-1,0,:] = 3,3,3,3
    NBinv = 1.0/NB
    #-------------

    #----Time marching-----
    for t in range(Nt):
        R = np.random.rand(N,N,m) #Random numbers used to update S every time step

        #Set up coefficients for fitness calculation
        A = np.ones((N,N,m))
        ind0 = np.where(S==0)
        A[ind0] = b

        #Add boundary of zeros to S
        S2[1:-1,1:-1,:] = S

        #Count number of C neighbors for each point
        NC = S2[:-2,:-2,:]+S2[:-2,1:-1,:]+S2[:-2,2:,:]+S2[1:-1,:-2,:] + S2[1:-1,2:,:] + S2[2:,:-2,:] + S2[2:,1:-1,:] + S2[2:,2:,:]

        #Calculate fitness matrix, F----
        F = NC*A
        F[ind0] = F[ind0] + (NB[ind0]-NC[ind0])*e
        F = F*NBinv
        #-----------

        #Calculate probability matrix, P-----
        F2[1:-1,1:-1,:]=F
        F2S2 = F2*S2
        #Total fitness of cooperators in community
        P = F2S2[:-2,:-2,:]+F2S2[:-2,1:-1,:]+F2S2[:-2,2:,:]+F2S2[1:-1,:-2,:] + F2S2[1:-1,1:-1,:] + F2S2[1:-1,2:,:] + F2S2[2:,:-2,:] + F2S2[2:,1:-1,:] + F2S2[2:,2:,:]

        #Total fitness of all members of community
        Pden = F2[:-2,:-2,:]+F2[:-2,1:-1,:]+F2[:-2,2:,:]+F2[1:-1,:-2,:] + F2[1:-1,1:-1,:] + F2[1:-1,2:,:] + F2[2:,:-2,:] + F2[2:,1:-1,:] + F2[2:,2:,:]

        P = (P/Pden)*g + 0.5*(1.0-g) #probability matrix
        #---------

        #Set new affiliations based on probability matrix and random numbers stored in R
        S[:,:,:] = 0
        S[R<=P] = 1

        fc_ave[t+1] = S.sum()
        #----Finish time marching-----

    fc_ave = fc_ave*N2inv/m

    return S,fc_ave
#------------------


def performance(input=(None),display=False):
    """Assess performance of simulate2, simulate2_f90, and simulate2_omp
    Modify the contents of the tuple, input, as needed
    When display is True, figures equivalent to those
    you are submitting should be displayed
    """
    if (input==(None)):
        return None
    else:
        tr.tr_b=input[2]    #set parameter values
        tr.tr_e=input[3]
        tr.tr_g=input[4]
        t_omp=np.zeros(input[6]) #initialize
        
        for i in range(input[6]):   #iterate between 1 and maximum number of threads
            tr.numthreads=i+1
            t0=time.time()  #time of start
            tr.simulate2_omp(input[0],input[1],input[5])
            t1=time.time()  #timwe of end
            t_omp[i]=t1-t0 #computation time
        speedup=np.round((t_omp/t_omp[0])**-1,2)    #calculate speedup
        print("OMP speedup:"+str(speedup)) 
        
        
        
        t0=time.time()  #time of start
        tr.simulate2_f90(input[0],input[1],input[5])

        t1=time.time()  #time of end
        t_f90=t1-t0     #computation time
        
        t0=time.time()  #time of start
        simulate2(input[0],input[1],input[2],input[3],input[4],input[5])
        t1=time.time()  #time of end
        t_py=t1-t0  #computation time
        
        te=np.append(t_omp,[t_f90,t_py])    #create array to plot
        te=np.round(te,2)   #round to 2 decimal places for clarity
        print('Time:' +str(te))
        if(display):#plotting
            plt.plot(['OMP-1thread','OMP-2threads','f90','Python'],te,'o')
            plt.ylabel('time [s]')
            plt.title('Time of execution: Nt=' +str(input[1])+', m='+str(input[5])+', N='+str(input[0])+'| A.Grzyb - performance' )
    
    
    
    return te,tr.simulate2_omp(input[0],input[1],input[5]) #Modify as needed
"""Irrespective on the relative size of N, Nt, m the relative time of execution stays constant: single thread is
    more or less equal to f_90, the speed up of 2 threads is around 2, Python takes approximately 4 times longer
    to execute than f_90. Single thread OMP is equal to f_90 because f_90 is utilizing single thread as well,
    the slight differences in execution time may stem from the fact that calling OMP changes something in the way
    program is executed. 2 threads are faster than 1 because we are effectively performing 2 calculations at once
    so the time required is halved. Python being an interpreted language takes significantly longer to execute than
    any Fortran piece, but this drawback is forgiveable when considering the reduced development time being a consequence
    of its convenience.
"""

def analyze(input=(None),display=False):
    """Analyze influence of model parameter, g.
    Modify the contents of the tuple, input, as needed
    When display is True, figures equivalent to those
    you are submitting should be displayed
    """
    if (input==(None)):
        return None
    else:
        N=51    #set parameters
        tr.tr_e=0.01
        arb=np.linspace(1.1,1.5,input[0])   #trying specified number of points in the range of b:[1.1,1.5]
        arg=np.linspace(0.8,1,input[1]) #trying specified number of points in the range of gamma:[0.8,1]
        plt.figure(figsize=(8, 6))
        plt.title('Aleksander Grzyb - analyze',fontsize=12)
        plt.xlabel('t',fontsize=14)
        plt.ylabel('fc(t)',fontsize=14)
        for g in arg:
            for b in arb:
                tr.tr_b=b
                tr.tr_g=g
                r=tr.simulate2_omp(N,input[2],input[3]) #simulate for the grid of b and gamma
                if(display):
                    plt.plot(r[1],label=('b = '+str(np.round(b,2))+', g = '+str(np.round(g,2))))    #plot f_ave
                    plt.legend(fontsize='small')
                    plt.hold(True)
        return None #Modify as needed
    """Setting gamma=1 takes us back to the situation analyzed in Homework 1, in which the larger the value of b
        the quicker and on average more often all villages will become M eventually, which we can notice on the plot.
        For gamma !=1 the situation resolves more quickly i.e. the proportion of C becomes stable. For gamma=0.8 and 
        all values of b the decay is maximal (i.e. one layer of C villages encircling the middle M's becomes C per timestep, although sometimes
        the initial M village becomes C and the system is stable). For middle values of gamma and b i.e. g=0.93
        b=1.1 the system takes a little longer but nevetheless stabilizes faster than for any graph with g=1.
    """
    
def visualize(N,Nt):
    tr.tr_b=1.1
    tr.tr_e=0.01    #set parameters
    tr.tr_g=0.95
    S=tr.simulatemov(N,Nt,1)[0] #call modified version of simulate2_f90 which returns an NxNx(Nt+1) array storing intermediate states
    S=np.transpose(S) #Fortran stores data in column major as opposed to Python's row major, transposition puts everything back in place
    ind_s0 = np.where(S[0]==0) #inspired by plot_S
    ind_s1 = np.where(S[0]==1)   
    fig, ax =plt.subplots()
    ax.set_xlim(-0.5,N-0.5)
    ax.set_ylim(-0.5,N-0.5)
    
    plt.hold(True)
    plot1,=ax.plot(ind_s0[1],'rs',animated=True)
    plot2,=ax.plot(ind_s1[1],'gs',animated=True)
    plot1,=ax.plot(ind_s0[0],'rs',animated=True)
    
    plot2,=ax.plot(ind_s1[0],'gs',animated=True)
    plt.axis('off')
    plt.title('Aleksander Grzyb - visualize()')
    def update(x):
        if(x<Nt+1):
            ind_s0 = np.where(S[x]==0) 
            ind_s1 = np.where(S[x]==1) 
            plot1.set_xdata(ind_s0[1])
            plot2.set_xdata(ind_s1[1])
            plot1.set_ydata(ind_s0[0])
            
            plot2.set_ydata(ind_s1[0])
        return plot1,plot2,
    ani=animation.FuncAnimation(fig, update,frames=Nt,interval=100,repeat=False,blit=True)
    ani.save('hw3movie1.mp4',writer='ffmpeg')
    return None


inp1=(51,49,1.1,0.01,0.95,100,2) #(N,Nt,b,e,g,m,numthreads)
inp2=(3,4,199,250) #npoints in b-range, npoints in gamma-range, Nt, m

if __name__ == '__main__':
    #Modify the code here so that it calls performance analyze and
    # generates the figures that you are submitting with your code

    input_p = inp1
    output_p = performance(input_p,True) #modify as needed


    input_a = inp2
    output_a = analyze(input_a,True)
    
#    visualize(21,199)
